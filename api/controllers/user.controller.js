const bcrypt=require("bcrypt");
const jwt=require("jsonwebtoken");
const mongoose=require("mongoose");
const User=require('../../database/models/user.model');
const UserService = require('../../services/user.service');
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'enuke.social@enukesoftware.com',
      pass: 'enuke@123'
    }
  });

class UserController{
    async user_signup(req,res){
        let user = await UserService.getUser(req);
        if(user.length >=1){
            return res.status(409).json({
                message:"Mail exists"
            });
        }else{
            bcrypt.hash(req.body.password,10,(err,hash)=>{
                if(err){
                    return res.status(500).json({
                        error:err
                    })
                }else{
                    const user=new User({
                        _id:new mongoose.Types.ObjectId(),
                        email:req.body.email,
                        password:hash
                    });
                    user
                    .save()
                    .then(result=>{
                        console.log(result);
                        res.status(201).json({
                            message:'User created'
                        });
                    })
                    .catch(err=>{
                        console.log(err);
                        res.status(500).json({
                            error:err
                        })
                    });
                }
            })
        }
    }
    async user_login(req,res){
        User.find({email:req.body.email})
        .exec()
        .then(user=>{
            if(user.length<1){
                return res.status(401).json({
                    message:'Auth field'
                })
            }
            bcrypt.compare(req.body.password,user[0].password,(err,result)=>{
                if(err){
                    return res.status(401).json({
                        message:'Auth failed',
                        error:err
                    })
                }
                if(result){
                    const token=jwt.sign({
                        email:user[0].email,
                        userId:user[0]._id
                    },
                    process.env.JWT_KEY,
                    {
                        expiresIn:"1h"
                    }
                    );
                    return res.status(200).json({
                        message:'Auth successfully',
                        token:token
                    });
                }
                res.status(401).json({
                    message:'Auth failed'
                })
            })
        })
        .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        })
    })
    }
    async forgotPassword(req,res){
        User.find({email:req.body.email})
        .exec()
        .then(user=>{
            if(user.length<1){
                return res.status(409).json({
                    message:"User with this email id does'nt exists."
                });
            }
            const token=jwt.sign({
                id:user[0]._id,
            },
            process.env.JWT_KEY_FORGOTPASS,
            {
                expiresIn:"1h"
            }
            );
            const data={
                from:'noreplay@hello.com',
                to:req.body.email,
                subject:'Reset Password Link',
                html:`
                <h2>Please click on given link to reset your password</h2>
                <p>${process.env.BASE_URL}user/reset-password/${token}</p>`
            };
            return User.updateOne({resetLink:token})
            .exec()
            .then(result=>{
                transporter.sendMail(data, function(error, info){
                    if (error) {
                    console.log(error);
                    res.status(400).json({
                        message:'Error in sending mail',    
                            error:error
                    })
                    } else {
                    console.log('Email sent: ' + info.response);
                    res.status(200).json({
                        message:"Email has been send,kindly folow the instructions",
                        info:info.response
                    })
                    }
                });
            })
            .catch(err=>{
                res.status(400).json({
                    message:'reset password link error',    
                    error:err
                })
            })

        })
        .catch()
    }
    async resetPassword(req,res){
        if(req.body.token){
            jwt.verify(req.body.token,process.env.JWT_KEY_FORGOTPASS,function(error,decodedData){
                if(error){
                    return res.status(401).json({
                        error:"Incorrect token or it's expired"
                    })
                }
                User.find({resetLink:req.body.token})
                .exec()
                .then(user=>{
                    if(user.length<1){
                        return res.status(400).json({
                            message:"User with this token does'nt exists."
                        });
                    }
                })
                bcrypt.hash(req.body.newPassword,10,(err,hash)=>{
                if(err){
                    return res.status(500).json({
                        error:err
                    })
                }else{
                    User.updateOne({password:hash,resetLink:''})
                    .exec()
                    .then(user=>{
                        res.status(200).json({
                            message:"your password has been changed"
                        })
                    })
                    .catch(err=>{
                        res.status(400).json({
                            error:err
                        })
                    })
                }
            })
            })
        }else{
            res.status(401).json({
                error:"Authentication error!!"
            })
        }
    }
    async user_delete(req,res){
        User.remove({_id:req.params.userId})
        .exec()
        .then(result=>{
            res.status(200).json({
                message:"User deleted"
            })
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        })
    }
}
module.exports=new UserController;
