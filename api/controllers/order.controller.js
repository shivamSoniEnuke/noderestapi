const mongoose=require('mongoose');
const Order=require('../../database/models/order.model');
const Product=require('../../database/models/product.model');

class OrderController{
   async order_get_all(req,res){
        Order
        .find()
        .select("_id quantity product")
        .populate("product","name")
        .exec()
        .then(docs=>{
            res.status(200).json({
                count:docs.length,
                orders:docs.map(doc=>{
                    return {
                        _id:doc._id,
                        product:doc.product,
                        quantity:doc.quantity,
                        request:{
                            type:'GET',
                            url:'localhost:3001/order/'+doc._id
                        }
                    }
                })

            });
        })
        .catch(err=>{
            res.status(500).json({
                error:err
            })
        })
   }
   async orders_create_order(req,res,next){
        Product.findById(req.body.productId)
        .then(product=>{
            if(!product){
                return res.status(404).json({
                    message:"Product not found"
                })
            }
            const order=new Order({
                _id:mongoose.Types.ObjectId(),
                product: req.body.productId,
                quantity:req.body.quantity,
            });
            return order.save()
        .then(result=>{
            console.log(result);
            res.status(201).json({
                message:'Order stored',
                createdOrder:{
                _id:result._id,
                product:result.product,
                quantity:result.quantity
                },
                request:{
                    type:'GET',
                    url:'localhost:3001/order/'+result._id
                }
            });
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });
        })
   }
   async orders_get_order(req,res){
        Order.findById(req.params.orderId)
        .populate("product")
        .exec()
        .then(order=>{
            if(!order){
                return res.status(404).json({
                    message:'Order not found'
                })
            }
            res.status(200).json({
                order:order,
                request:{
                    type:"GET",
                    url:"localhost:3001/order/"
                }
            })
        })
        .catch(err=>{
            res.status(500).json({
                error:err
            })
        })
   }
   async orders_delete_order(req,res,next){
        Order.remove({_id:req.params.orderId})
        .exec()
        .then(result=>{
        res.status(200).json({
            message:"order deleted",
            request:{
                type:"POST",
                url:"localhost:3001/order/",
                body:{productId:'ID',quantity:'Number'}
            }
        })
        })
        .catch(err=>{
            res.status(500).json({
                error:err
            })
        })
    }

}

module.exports = new OrderController()
