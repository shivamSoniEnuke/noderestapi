const express=require("express");
const router=express.Router();
const multer=require('multer');
const checkAuth=require('../middleware/check-auth');

const storage=multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,'./uploads/product');
    },
    filename:function(req,file,cb){
        cb(null,new Date().toISOString()+file.originalname);
    }
})

const fileFilter=(req,file,cb)=>{
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null,true);
    }else{
        cb(null,false);
    }
}

const upload=multer({storage:storage,
    limits:{
            fileSize:1024*1024*5
        },
    fileFilter:fileFilter
});
const produtController=require('../controllers/product.controller');
router.get('/',produtController.products_get_all);
router.post('/',checkAuth,upload.single('productImage'),produtController.products_create_product);
router.get('/:productId',produtController.producs_get_product);
router.patch('/:productId',checkAuth,produtController.products_update_product);
router.delete('/:productId',checkAuth,produtController.products_delete_product);

module.exports=router;